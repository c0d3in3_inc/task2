package com.c0d3in3.task2

import android.app.Activity
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_user.*

class UserActivity : AppCompatActivity() {

    private lateinit var userModel : UserModel
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_user)

        init()
    }

    private fun init(){
        userModel = intent.extras?.get("userModel") as UserModel

        updateUI() // updating UI with received userModel

        saveButton.setOnClickListener {
            if(checkFields()) saveUser()
            else Toast.makeText(this, "Please fill all the fields!", Toast.LENGTH_SHORT).show()
        }

        cancelButton.setOnClickListener {
            cancelChanges()
        }

    }
    private fun updateUI(){
        firstNameEditText.text = userModel.firstName.toEditable()
        lastNameEditText.text = userModel.lastName.toEditable()
        emailEditText.text = userModel.email.toEditable()
    }

    private fun saveUser(){
        val userModel = UserModel(firstNameEditText.text.toString(), lastNameEditText.text.toString(), emailEditText.text.toString())
        intent.putExtra("userModel", userModel)
        val position = intent.extras?.get("position") as Int
        intent.putExtra("position", position)
        setResult(Activity.RESULT_OK, intent)
        finish()
    }

    private fun cancelChanges(){
        finish()
    }

    private fun checkFields() : Boolean{
        if(firstNameEditText.text.isNotEmpty() && lastNameEditText.text.isNotEmpty() && emailEditText.text.isNotEmpty()) return true
        return false
    }


    private fun String.toEditable(): Editable =  Editable.Factory.getInstance().newEditable(this) // editable to string
}
