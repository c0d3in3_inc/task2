package com.c0d3in3.task2

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_users.*

class UsersActivity : AppCompatActivity(), UsersAdapter.UsersInterface {

    companion object{
        const val REQUEST_CODE = 22
    }

    private val usersList = mutableListOf<UserModel>()
    private val adapter = UsersAdapter(usersList,this)
    private val firstNames = arrayOf("Gela", "Jemala", "Vano","Giorgi", "Dato", "Ana") // random first Names
    private val lastNames = arrayOf("White", "Beridze", "Black", "Orange", "Obama") // random last  Names
    private val emails = arrayOf("gela@mail.ru", "jemala@gmail.com", "hunterx@gmail.com", "ragacmail@gmail.com") // random emails
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_users)

        init()
    }

    private fun init(){
        usersRecyclerView.layoutManager = LinearLayoutManager(this)

        usersRecyclerView.adapter = adapter

        addUsers() // adding first users by default

        addUserButton.setOnClickListener {
            addUser()
        }
    }

    private fun addUsers(){ // adding 6 users
        for(user in 0..5){
            usersList.add(UserModel(firstNames.random(), lastNames.random(), emails.random()))
        }
        adapter.notifyDataSetChanged()
    }

    private fun addUser(){
        usersList.add(UserModel(firstNames.random(), lastNames.random(), emails.random()))
        adapter.notifyItemInserted(usersList.size-1)
        usersRecyclerView.scrollToPosition(usersList.size-1) //scroll to newly added user position
    }

    override fun editItem(position: Int) {
        val intent = Intent(this, UserActivity::class.java)
        val userModel = UserModel(usersList[position].firstName, usersList[position].lastName, usersList[position].email)
        intent.putExtra("userModel", userModel)
        intent.putExtra("position", position)
        startActivityForResult(intent, REQUEST_CODE)
    }

    override fun removeItem(position: Int) {
        usersList.removeAt(position)
        adapter.notifyItemRemoved(position)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if(resultCode == Activity.RESULT_OK && requestCode == REQUEST_CODE){
            val userModel = data?.extras?.get("userModel") as UserModel
            val position = data?.extras?.get("position") as Int

            usersList[position].firstName = userModel.firstName
            usersList[position].lastName = userModel.lastName
            usersList[position].email = userModel.email

            adapter.notifyItemChanged(position)
        }
        super.onActivityResult(requestCode, resultCode, data)
    }
}
