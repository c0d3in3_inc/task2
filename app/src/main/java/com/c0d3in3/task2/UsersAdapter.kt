package com.c0d3in3.task2

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.user_list_item_layout.view.*

class UsersAdapter(private val users : MutableList<UserModel>, listener : UsersInterface) : RecyclerView.Adapter<UsersAdapter.ViewHolder>() {

    interface UsersInterface{
        fun editItem(position: Int)
        fun removeItem(position: Int)
    }

    private val callback = listener

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.user_list_item_layout,parent, false))
    }

    override fun getItemCount(): Int = users.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.onBind()
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){
        fun onBind(){
            itemView.firstNameTextView.text = "First name: ${users[adapterPosition].firstName}"
            itemView.lastNameTextView.text = "Last name: ${users[adapterPosition].lastName}"
            itemView.emailTextView.text = users[adapterPosition].email

            itemView.editButton.setOnClickListener {
                callback.editItem(adapterPosition)
            }

            itemView.removeButton.setOnClickListener {
                callback.removeItem(adapterPosition)
            }
        }
    }
}